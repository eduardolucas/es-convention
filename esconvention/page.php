<?php get_header(); ?>

<section id="page-top" role="main">

      <article class="container main-container margin-section">

        <?php the_breadcrumb(); ?>

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                  <header class="margin-section">
                    <h1>
                      <?php the_title(); ?>
                    </h1>
                  </header>

                  <div class="menu">
                     <?php the_content(); ?>
                  </div>

            <?php endwhile; ?>
            <?php endif; ?>
      </article>

      <?php get_sidebar(); ?>

</section>

<?php get_footer(); ?>
