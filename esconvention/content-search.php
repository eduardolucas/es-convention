<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'newshometh', array('class' => 'pull-left news-thumb')); ?></a>

<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
  <h3>
    <?php the_title(); ?>
  </h3>
</a>

<small class="single-excerpt">
  <?php the_excerpt(); ?>
  <a href="<?php the_permalink(); ?>">Leia Mais...</a>
</small>

<hr>
