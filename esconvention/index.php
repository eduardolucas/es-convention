<?php get_header(); ?>

<!-- main -->
<section role="main">

  <!-- news -->
  <section class="news margin-section" id="noticias">
     <div class="container main-container">
        <div class="row">
           <h3 class="text-center text-uppercase news-title">
            Notícias
           </h3>
           <?php
               $count=0;
               query_posts( array ( 'category_name' => 'noticias', 'posts_per_page' => 4 ) );
               while (have_posts()) : the_post();
           ?>
           <article class="col-xs-12 col-sm-6 news-item">
              <a href="<?php the_permalink(); ?>#page-top">
                 <?php the_post_thumbnail( 'newshometh', array('class' => 'pull-left news-thumb')); ?>
              </a>

              <div class="news-item-text">
                <h4><a href="<?php the_permalink(); ?>#page-top">
                   <?php the_title(); ?>
                </a></h4>

                <p><a href="<?php the_permalink(); ?>#page-top">
                   <?php the_excerpt(); ?>
                </a></p>
              </div>
           </article>
           <?php
              $count++;
              if($count == 2 || $count == 4 ) echo '</div><div class="row">';
              endwhile;
           ?>
         </div> <!-- row -->
      </div>    <!-- container -->
  </section>

  <!-- multimedia -->
  <section class="multimedia margin-section" id="galeria">
     <?php masterslider(1); ?>
     <div class="container tiny-container text-center">
       <div class="multimedia-btn">
         <a target="_blank" href="<?php echo get_theme_mod('esconvention_galleryurl', ''); ?>">
           <span class="text-uppercase">
             <?php echo get_theme_mod('esconvention_gallerytext', ''); ?>
           </span>
         </a>
       </div>
     </div>
  </section>

  <?php get_sidebar(); ?>

</section>

<?php get_footer(); ?>
