<!-- footer -->
<footer role="contentinfo" id="main-footer">

   <section id="contato" class="footer-contact">
      <div class="container">
         <div class="row footer-container">
               <h4 class="contact-title col-xs-12 text-center">
                 <?php echo get_theme_mod('esconvention_contact', ''); ?>
               </h4>
               <section class="contact-info col-xs-12 col-sm-6 text-left">
               <adress>
                 <?php if ( is_active_sidebar( 'contact_box' ) ) : ?>
                 <?php dynamic_sidebar( 'contact_box' ); ?>
                 <?php endif; ?>
               </adress>
               <ul class="list-inline contact-social-icons">
                 <li>
                     <a target="_blank" href="https://www.facebook.com/ESConventionBureau">
                     <span class="fa-stack">
                     <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                     <i class="fa fa-facebook fa-stack-1x" aria-hidden="true"></i>
                     </span></a>
                 </li>
               </ul>
            </section>
            <section class="col-xs-12 col-sm-6">
               <?php echo do_shortcode( '[contact-form-7 id="67" title="Form Contato"]' ); ?>
            </section>
         </div>
      </div>
   </section>

   <section class="footer-credit text-center">
      <div class="footer-info">
         <small class="copyright-info">
           <?php echo get_theme_mod('esconvention_copyright', ''); ?>
         </small>
      </div>
   </section>

</footer>

    <!-- fixed back to top -->

    <span id="top-link-block" class="hidden">
        <a href="#top" class="well well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
            <i class="glyphicon glyphicon-chevron-up"></i>
        </a>
    </span><!-- /top-link-block -->

    <?php wp_footer(); ?>

</body>
</html>
