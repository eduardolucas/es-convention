<?php get_header(); ?>

<section id="page-top" role="main">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
     <article class="single-post margin-section">
      <div class="container main-container">
        <?php the_breadcrumb(); ?>
        <header>
         <h2 class="single-title">
            <?php the_title(); ?>
            <small class="single-excerpt">
              <?php the_excerpt(); ?>
            </small>
         </h2>
         <hr>
         <ul class="list-inline text-right single-share">
            <li>
                <?php if ( function_exists( 'sharing_display' ) ) {
                  sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                  $custom_likes = new Jetpack_Likes;
                  echo $custom_likes->post_likes( '' );
                }
                ?>
            </li>
         </ul>
       </header>
          <?php the_content(); ?>
      </div>
     </article>
  <?php endwhile; ?>
  <?php else : ?>
  <?php endif; ?>

  <?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>
