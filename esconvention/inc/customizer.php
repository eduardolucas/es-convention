<?php
/**
 * esconvention 1.0 Theme Customizer support
 *
 * @package WordPress
 * @subpackage esconvention
 * @since Longform 1.0
 */

function esconvention_customize_register( $wp_customize ) {

	// Opções Gerais

	$wp_customize->add_panel( 'esconvention_general_panel', array(
		'priority'       => 250,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Opções gerais' , 'esconvention'),
		'description'    => __( 'Configure aqui as opções gerais do seu site.' , 'esconvention')
	) );



	// Imagem fundo header

	$wp_customize->add_section( 'esconvention_general_headerbg', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Imagem de fundo do Cabeçalho' , 'esconvention'),
		'description'    => __( 'Atualize aqui a imagem de fundo do cabeçalho' , 'esconvention'),
		'panel'          => 'esconvention_general_panel'
	) );

	$wp_customize->add_setting( 'esconvention_headerbg', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_headerbg', array(
		'label'    => __( 'Imagem de Fundo', 'esconvention' ),
		'section'  => 'esconvention_general_headerbg',
		'settings' => 'esconvention_headerbg',
	) ) );

		// Texto botão galeria
		$wp_customize->add_section( 'esconvention_general_gallerytext', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Texto do botão da Galeria' , 'esconvention'),
			'description'    => __( 'Insira o texto a ser exibido no botão da Galeria.' , 'esconvention'),
			'panel'          => 'esconvention_general_panel'
		) );

		$wp_customize->add_setting( 'esconvention_gallerytext', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'esconvention_gallerytext',
			array(
				'label'      => 'Texto do botão',
				'section'    => 'esconvention_general_gallerytext',
				'type'       => 'text',
			)
		);

		// URL botão galeria
		$wp_customize->add_section( 'esconvention_general_galleryurl', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'URL do botão da Galeria' , 'esconvention'),
			'description'    => __( 'Insira o url do botão da Galeria.' , 'esconvention'),
			'panel'          => 'esconvention_general_panel'
		) );

		$wp_customize->add_setting( 'esconvention_galleryurl', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'esconvention_galleryurl',
			array(
				'label'      => 'URL do botão',
				'section'    => 'esconvention_general_galleryurl',
				'type'       => 'text',
			)
		);

		// Mapa
		$wp_customize->add_section( 'esconvention_general_map', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Código do Mapa' , 'esconvention'),
			'description'    => __( 'Insira o código de incorporação (embed code) do Mapa.' , 'esconvention'),
			'panel'          => 'esconvention_general_panel'
		) );

		$wp_customize->add_setting( 'esconvention_map', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'esconvention_map',
			array(
				'label'      => 'Código do mapa',
				'section'    => 'esconvention_general_map',
				'type'       => 'text',
			)
		);

		// Contatos
		$wp_customize->add_section( 'esconvention_general_contact', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Título Seção Contatos' , 'esconvention'),
			'description'    => __( 'Insira o título da seção Contatos.' , 'esconvention'),
			'panel'          => 'esconvention_general_panel'
		) );

		$wp_customize->add_setting( 'esconvention_contact', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'esconvention_contact',
			array(
				'label'      => 'Título da seção',
				'section'    => 'esconvention_general_contact',
				'type'       => 'text',
			)
		);

		// Copyright
		$wp_customize->add_section( 'esconvention_general_copyright', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Texto Copyright' , 'esconvention'),
			'description'    => __( 'Insira o texto de Copyright.' , 'esconvention'),
			'panel'          => 'esconvention_general_panel'
		) );

		$wp_customize->add_setting( 'esconvention_copyright', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'esconvention_copyright',
			array(
				'label'      => 'Texto de Copyright',
				'section'    => 'esconvention_general_copyright',
				'type'       => 'text',
			)
		);

	// Conteúdos Especiais

	$wp_customize->add_panel( 'esconvention_special_panel', array(
		'priority'       => 253,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Conteúdos Especiais' , 'esconvention'),
		'description'    => __( 'Configure aqui as opções da seção de Conteúdos Especiais.' , 'esconvention')
	) );

	// Imagem serviço 1
	$wp_customize->add_section( 'esconvention_general_specialimg', array(
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Conteúdos Especiais - Configurações' , 'esconvention'),
		'description'    => __( 'Configure aqui as opções de imagem, url, e texto alternativo da seção Conteúdos Especiais. ' , 'esconvention'),
		'panel'          => 'esconvention_special_panel'
	) );

	// Imagem 1
	$wp_customize->add_setting( 'esconvention_specialimg1', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg1', array(
		'label'    => __( 'Imagem Serviço 1', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg1',
	) ) );

	// URL 1
	$wp_customize->add_setting( 'esconvention_specialurl1', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl1',
		array(
			'label'      => 'URL da Imagem 1',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 1
	$wp_customize->add_setting( 'esconvention_specialalt1', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt1',
		array(
			'label'      => 'Texto alternativo da Imagem 1',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Imagem 2
	$wp_customize->add_setting( 'esconvention_specialimg2', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg2', array(
		'label'    => __( 'Imagem Serviço 2', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg2',
	) ) );

	// URL 2
	$wp_customize->add_setting( 'esconvention_specialurl2', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl2',
		array(
			'label'      => 'URL da Imagem 2',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 2
	$wp_customize->add_setting( 'esconvention_specialalt2', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt2',
		array(
			'label'      => 'Texto alternativo da Imagem 2',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Imagem 3
	$wp_customize->add_setting( 'esconvention_specialimg3', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg3', array(
		'label'    => __( 'Imagem Serviço 3', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg3',
	) ) );

	// URL 3
	$wp_customize->add_setting( 'esconvention_specialurl3', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl3',
		array(
			'label'      => 'URL da Imagem 3',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 3
	$wp_customize->add_setting( 'esconvention_specialalt3', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt3',
		array(
			'label'      => 'Texto alternativo da Imagem 3',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Imagem 4
	$wp_customize->add_setting( 'esconvention_specialimg4', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg4', array(
		'label'    => __( 'Imagem Serviço 4', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg4',
	) ) );

	// URL 4
	$wp_customize->add_setting( 'esconvention_specialurl4', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl4',
		array(
			'label'      => 'URL da Imagem 4',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 4
	$wp_customize->add_setting( 'esconvention_specialalt4', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt4',
		array(
			'label'      => 'Texto alternativo da Imagem 4',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Imagem 5
	$wp_customize->add_setting( 'esconvention_specialimg5', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg5', array(
		'label'    => __( 'Imagem Serviço 5', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg5',
	) ) );

	// URL 5
	$wp_customize->add_setting( 'esconvention_specialurl5', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl5',
		array(
			'label'      => 'URL da Imagem 5',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 5
	$wp_customize->add_setting( 'esconvention_specialalt5', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt5',
		array(
			'label'      => 'Texto alternativo da Imagem 5',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Imagem 6
	$wp_customize->add_setting( 'esconvention_specialimg6', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'esconvention_specialimg6', array(
		'label'    => __( 'Imagem Serviço 6', 'esconvention' ),
		'section'  => 'esconvention_general_specialimg',
		'settings' => 'esconvention_specialimg6',
	) ) );

	// URL 6
	$wp_customize->add_setting( 'esconvention_specialurl6', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialurl6',
		array(
			'label'      => 'URL da Imagem 6',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// alt 6
	$wp_customize->add_setting( 'esconvention_specialalt6', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'esconvention_specialalt6',
		array(
			'label'      => 'Texto alternativo da Imagem 6',
			'section'    => 'esconvention_general_specialimg',
			'type'       => 'text',
		)
	);

	// Add General setting panel and configure settings inside it
	$wp_customize->add_panel( 'esconvention_espiritosanto_panel', array(
		'priority'       => 252,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Espírito Santo' , 'esconvention'),
		'description'    => __( 'Configure aqui as opções da seção Espírito Santo.' , 'esconvention')
	) );

}

add_action( 'customize_register', 'esconvention_customize_register' );

if ( class_exists( 'WP_Customize_Section' ) && !class_exists( 'esconvention_Customized_Section' ) ) {
	class esconvention_Customized_Section extends WP_Customize_Section {
		public function render() {
			$classes = 'accordion-section control-section control-section-' . $this->type;
			?>
			<li id="accordion-section-<?php echo esc_attr( $this->id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
				<style type="text/css">
					.cohhe-social-profiles {
						padding: 14px;
					}
					.cohhe-social-profiles li:last-child {
						display: none !important;
					}
					.cohhe-social-profiles li i {
						width: 20px;
						height: 20px;
						display: inline-block;
						background-size: cover !important;
						margin-right: 5px;
						float: left;
					}
					.cohhe-social-profiles li i.cohhe_logo {
						background: url(<?php echo get_template_directory_uri().'/images/icons/cohhe.png'; ?>);
					}
					.cohhe-social-profiles li a {
						height: 20px;
						line-height: 20px;
					}
					#customize-theme-controls>ul>#accordion-section-longform_social_links {
						margin-top: 10px;
					}
					.cohhe-social-profiles li.documentation {
						text-align: right;
						margin-bottom: 10px;
					}
					.cohhe-social-profiles li.gopremium {
						text-align: right;
						margin-bottom: 60px;
					}
				</style>
			</li>
			<?php
		}
	}
}


function esconvention_sanitize_checkbox( $input ) {
	// Boolean check
	return ( ( isset( $input ) && true == $input ) ? true : false );
}

/*
 * Bind JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function esconvention_customize_preview_js() {
	wp_enqueue_script( 'esconvention_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20131205', true );
}
add_action( 'customize_preview_init', 'esconvention_customize_preview_js' );
