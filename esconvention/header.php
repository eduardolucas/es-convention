<!doctype html>

<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?php if (function_exists('is_tag') && is_tag()) { echo 'Assunto &quot;'.$tag.'&quot; • '; } elseif (is_archive()) { wp_title(''); echo ' • '; } elseif (is_search()) { echo 'Resultados da busca por &quot;'.wp_specialchars($s).'&quot; • '; } elseif (!(is_404()) && (is_single()) || (is_page())) { wp_title(''); echo ' • '; } elseif (is_404()) { echo 'Não encontrado • '; } if (is_home()) { bloginfo('name'); echo ' • '; bloginfo('description'); } else { bloginfo('name'); } ?>
  </title>
  <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/apple-touch-icon.png">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.png">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head(); ?>
</head>
<body id="top">

  <?php
       $headerbg = get_theme_mod('esconvention_headerbg', array());
  ?>

  <!-- header -->
  <header role="banner" id="general-header" style="background: url( '<?php echo $headerbg; ?>' ) no-repeat center; background-size: cover;
  background-attachment: fixed;" class="main-header">

     <!-- fixed top navbar -->
     <section id="navbar">
        <nav id="top_bar" class="navbar-fixed-top container-fluid black-bar">
           <ul class="list-inline text-right">
              <li>
                  <a target="_blank" href="https://www.facebook.com/ESConventionBureau"><span class="fa-stack fa-lg">
                  <i class="fa fa-square fa-stack-2x fa-inverse"></i>
                  <i class="fa fa-facebook fa-stack-1x" aria-hidden="true"></i>
                  </span>
                  </a>
              </li>
              <li>
                  <form class="searchform form-inline search-form" method="get" action="<?php echo esc_url( home_url() ); ?>/">
                  <input type="text" name="s" id="s" class="search-input text-uppercase text-center" placeholder="pesquisa" value="<?php the_search_query(); ?>">
                  <button id="searchsubmit" type="submit" class="text-uppercase text-center search-submit" value="">ok</button>
                  </form>
              </li>
           </ul>
        </nav>

        <nav role="navigation" id="main_bar" class="navbar navbar-fixed-top blue-bar">
           <div class="container-fluid">
               <div class="navbar-header">
                   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                   </button>

                   <a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo.png" alt="Logotipo ES Convention Visitors Bureau">
                   </a>
               </div>

               <div class="collapse navbar-collapse main-nav-collapse" id="main-navbar-collapse">
                 <?php
                       wp_nav_menu( array(
                           'menu'              => 'primary',
                           'theme_location'    => 'primary',
                           'depth'             => 2,
                           'container'         => '',
                           'container_class'   => '',
                           'container_id'      => '',
                           'menu_class'        => 'nav navbar-nav list-inline text-uppercase',
                           'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                           'walker'            => new wp_bootstrap_navwalker())
                       );
                 ?>
               </div>
           </div>
        </nav>

     </section>

     <h1 class="container tiny-container text-center header-title">
        Traga seu evento<br> para o Espírito Santo
     </h1>
  </header>
