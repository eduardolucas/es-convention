	  <header>
		    <h1>
           <?php _e( 'Nada encontrado', 'esconvention' ); ?>
        </h1>
	  </header><!-- .page-header -->

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p>
        <?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'esconvention' ), esc_url( admin_url( 'post-new.php' ) ) ); ?>
      </p>

		<?php elseif ( is_search() ) : ?>

			<p>
        <?php _e( 'Nada foi encontrado com o termo pesquisado. Tente novamente utilizando outras palavras-chave.', 'esconvention' ); ?>
      </p>

		<?php else : ?>

			<p>
        <?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'esconvention' ); ?>
      </p>

		<?php endif; ?>
