<?php get_header(); ?>

<section role="main">

      <article class="container main-container margin-section">

        <?php if (have_posts() ) : ?>

           <header class="margin-section">
              <h1>
                 <?php printf( __( 'Resultados da busca por: %s', 'esconvention' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
              </h1>
           </header>

        <?php
      	// Start the loop.
      	while ( have_posts() ) : the_post();

      	/**
      	* Run the loop for the search to output the results.
      	* If you want to overload this in a child theme then include a file
      	* called content-search.php and that will be used instead.
      	*/
      	get_template_part( 'content', 'search' );

      	// End the loop.
      	endwhile;

      	// Previous/next page navigation.
      	the_posts_pagination( array(
      		'prev_text'          => __( 'Previous page', 'esconvention' ),
      		'next_text'          => __( 'Next page', 'esconvention' ),
      		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'esconvention' ) . ' </span>',
      		) );

        // If no content, include the "No posts found" template.
        else :
        get_template_part( 'content', 'none' );

        endif;
        ?>

      </article>

      <?php get_sidebar(); ?>

</section>

<?php get_footer(); ?>
