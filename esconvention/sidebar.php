<!-- special content -->
<section class="special-content">
   <div class="container main-container">
      <div class="row">
        <a href="<?php echo get_theme_mod('esconvention_specialurl1', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg1', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt1', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>
        <a href="<?php echo get_theme_mod('esconvention_specialurl2', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg2', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt2', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>
        <a href="<?php echo get_theme_mod('esconvention_specialurl3', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg3', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt3', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>
        <a href="<?php echo get_theme_mod('esconvention_specialurl4', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg4', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt4', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>
        <a href="<?php echo get_theme_mod('esconvention_specialurl5', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg5', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt5', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>
        <a href="<?php echo get_theme_mod('esconvention_specialurl6', ''); ?>">
         <img src="<?php echo get_theme_mod('esconvention_specialimg6', ''); ?>" alt="<?php echo get_theme_mod('esconvention_specialalt6', ''); ?>" class="col-xs-6 col-sm-4 special-content-img">
        </a>


      </div>
   </div>
</section>

<!-- map -->
<section class="embed-map">
  <div class="embed-overlay" onClick="style.pointerEvents='none'"></div>
  <?php echo get_theme_mod('esconvention_map', ''); ?>
  <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1CeAYQvrKr_3j9Rh_e0CWK-qWgG8" width="100%" height="260"></iframe>
</section>
