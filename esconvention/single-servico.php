<?php get_header(); ?>

<section id="page-top" role="main">
    <div class="margin-section container main-container">
      <article>
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <ul class="breadcrumb margin-section">
          <li><a href="<?php echo esc_url( home_url() ); ?>" title="Home"><span class="glyphicon glyphicon-home"></span></a></li>
          <li>
            <a href="<?php echo esc_url( home_url() ); ?>/servicos/#page-top">
            <?php $post_type = get_post_type_object( get_post_type($post) );
            echo $post_type->label ; ?>
            </a>
          </li>
          <li>
            <?php the_title(); ?>
          </li>
        </ul>

            <header>
             <h2 class="single-title">
                <?php the_title(); ?>
             </h2>
             <hr>
             <ul class="list-inline text-right single-share">
                <li>
                    <?php if ( function_exists( 'sharing_display' ) ) {
                      sharing_display( '', true );
                    }

                    if ( class_exists( 'Jetpack_Likes' ) ) {
                      $custom_likes = new Jetpack_Likes;
                      echo $custom_likes->post_likes( '' );
                    }
                    ?>
                </li>
             </ul>
           </header>
              <?php the_content(); ?>
      <?php endwhile; ?>
      <?php else : ?>
      <?php endif; ?>
      </article>
    </div>
  <?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>
