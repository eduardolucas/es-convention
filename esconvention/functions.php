<?php
/*
 * Author: Buzz
 * URL: agenciabuzz.me
 * ES Convention & Visitors Bureau Theme functions
 *
 */

// Load scripts
function esconvention_header_scripts() {

    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
    wp_enqueue_script('bootstrap-js');

    // register modernizr

    wp_register_script('esconventionjs', get_template_directory_uri() . '/js/script.js', array('jquery'), '0.1', true);
    wp_enqueue_script('esconventionjs');
}

// Load styles
function esconvention_styles() {

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '', 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '', 'all');
    wp_enqueue_style('fontawesome');

    wp_register_style('esconvention', get_template_directory_uri() . '/style.css', array(), '', 'all');
    wp_enqueue_style('esconvention');
}

// Add thumbnails theme support
if ( function_exists( 'add_theme_support' ) )
{
  	add_theme_support( 'post-thumbnails' );
    	// set_post_thumbnail_size( width, height, true );
      add_image_size('newshometh', 64, 64, true);
}

// Add Theme Customizer functionality
require get_template_directory() . '/inc/customizer.php';

// Admin Javascript
add_action( 'admin_enqueue_scripts', 'esconvention_admin_scripts' );
function esconvention_admin_scripts() {
	wp_register_script('master', get_template_directory_uri() . '/js/admin-master.js', array('jquery'));
	wp_enqueue_script('master');
}

// Custom CF7 loading image
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
  return  get_template_directory_uri() . '/assets/ajax-loader.svg';
}

// Register custom navwalker
require_once('wp_bootstrap_navwalker.php');

// Bootstrap navwalker
class description_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $description  = ! empty( $item->description ) ? '<span class="at-item-txt">'.esc_attr( $item->description ).'</span></h1>' : '';

        if($depth != 0) {
            $description = $append = $prepend = "";
        }

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $description.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

// Register menu
register_nav_menus( array(
    'primary' => __( 'Menu Principal', 'esconvention' ),
) );

// Register Widgets
function esconvention_widgets_init() {

  register_sidebar( array(
		'name'          => 'Quick Bar - Conheça o ES (vídeo)',
		'id'            => 'video_quick_bar',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => 'Quick Bar - Notícias',
		'id'            => 'news_quick_bar',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

  register_sidebar( array(
		'name'          => 'Quick Bar - Eventos',
		'id'            => 'calendar_quick_bar',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

  register_sidebar( array(
		'name'          => 'Contatos',
		'id'            => 'contact_box',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

}

// breadcrumbs

function the_breadcrumb() {
    global $post;
    echo '<ul class="breadcrumb margin-section">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '"';
        echo 'title="Home">';
        echo '<span class="glyphicon glyphicon-home">';
        echo '</a></li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li> ');
            if (is_single()) {
                echo '</li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li>' . $output ;
                }
                echo $output;
                echo '<li class="active">'.$title.'</li>';
            } else {
                echo '<li class="active">'.get_the_title().'</li>';
            }
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}

// Remove jetpack automatic share buttons
function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

// add_filter( 'mctb_show_bar', function( $show ) {
//     return !is_page() and !is_single();
// } );

// Add Actions
add_action( 'init', 'esconvention_header_scripts' );
add_action( 'wp_enqueue_scripts', 'esconvention_styles' );
add_action( 'widgets_init', 'esconvention_widgets_init' );
add_action( 'loop_start', 'jptweak_remove_share' );

?>
