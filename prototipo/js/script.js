// back to top

if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('hidden').affix({
        // how far to scroll down before link "slides" into view
        offset: {top:100}
    });
}


// header shrinks when scroll page

$(window).on("scroll touchmove", function () {
    $('#top_bar').toggleClass('tiny', $(document).scrollTop() > 100);

    $('#main_bar').toggleClass('tiny', $(document).scrollTop() > 100);
});

// smooth scroll page link anchor

$(function() {
  $('a[href*="#"][class*="smooth"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// onclick copllapse in navbar menu

$('.nav a').on('click', function(){
    $('.navbar-toggle').click() //bootstrap 3.x by Richard
});

$(function() {
    $( "#accordion1" ).accordion({collapsible: true, active: false, heightStyle: "fill"});
    $( "#accordion2" ).accordion({collapsible: true, active: false});
    $( "#accordion3" ).accordion({collapsible: true, active: false});
});
